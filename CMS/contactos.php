<?php include('header.php') ?>

    <?php
    
    $del_id = isset($_REQUEST['delId']) ? $_REQUEST['delId'] : false;
    $affected_rows = 0;
    if ($del_id) {
        
        $query = 'DELETE FROM contactos WHERE id = ' . $del_id;
        $sucesso = mysqli_query($link, $query);
        
        if ($sucesso) {
            $affected_rows = mysqli_affected_rows($link);   
        }
    }

     $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;

    if ($sort) {
        
        $sortArray = explode(',', $sort);
        
        
        mysqli_query($link, 'START TRANSACTION');
        
        $sort_success = true;
        
        foreach($sortArray as $sort_order => $row_id) {
            $query_sort = "UPDATE contactos SET ordem = $sort_order WHERE id = $row_id";
            if (!mysqli_query($link, $query_sort)) {
                $sort_success = false;
                break;
            }            
        }
        
        
        if ($sort_success) {
            mysqli_query($link, 'COMMIT');    
        } else {
            mysqli_query($link, 'ROLLBACK'); 
        }   
    }

?>

        <div id="divTableEmenta" class="col-md-9" "col-sm-9" "col-xs-9">

            <br>

            <button id="btnSaveOrder" class="btn btn-default center-block" disabled>Guardar Nova Ordem</button>

            <br>

            <table id="tableGeral" class="table table-hover" class="align-center">

                <?php
    
    if ($del_id) {
        if ($affected_rows > 0) {
            echo '<div class="alert alert-success text-center" role="alert">Apagado com sucesso</div>';

        } 
    }
    
    
    ?>
                    <thead>
                        <tr>
                            <th>Contactos</th>
                        </tr>
                    </thead>
                    <?php 
             $query_contactos = 'SELECT * FROM contactos ORDER by ordem ASC';
             

             $result_contactos = mysqli_query($link, $query_contactos);
        
               echo '<tbody>';    
            
                while ($row_contactos = mysqli_fetch_array($result_contactos)) {  
                    
                        echo '<tr data-sort-element-id=' . $row_contactos['id'] . '>';

                        echo '<td>' . $row_contactos['descricao'] . '</td>';

                        echo '<td>' . $row_contactos['conteudo'] . '</td>'; 
                    
                        echo '<td> <a class="btn btn-info btnMove">Mover</a></td> ';
                        echo '<td><button class="btn btn-danger button-del" type="button" data-del-id="'.$row_contactos['id'].'">Apagar</button></td>';
                        echo '<td><a class="btn btn-default" href="contactos_detail.php?id='.$row_contactos['id'].'" role="button">Editar</a></td>';
                        
                        echo '</tr>'; 
                    
                        
                }
                echo '</tbody>';
        ?>

            </table>

            <a id="newEditBtn" class="btn btn-primary" href="contactos_detail.php" role="button">Novo Contacto</a>
            <br>
            <br>

        </div>
        <div style="clear: both"></div>
        <?php include('footer.php') ?>