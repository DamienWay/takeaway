<?php include('header.php'); ?>


    <?php
    
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;

$saved = isset($_REQUEST['saved']);

$success = true;

if ($saved) { 
    
        $pratos = $_REQUEST['pratos'];
        
        $query_del = 'DELETE FROM ementa_has_pratos WHERE ementa_id = ' . $id;
        if (!mysqli_query($link, $query_del)) {
            $success = false;
        }
        
        foreach($pratos as $pratos_id) {
            $query_ins = "INSERT INTO ementa_has_pratos (ementa_id, pratos_id) VALUES ($id, $pratos_id)";    
            if (!mysqli_query($link, $query_ins)) {
                $success = false;
                break;
            }
        }
    }


        $query_dia = 'SELECT * FROM ementa';
        $result_dia = mysqli_query($link, $query_dia);

        
        $query_cat = 'SELECT * FROM categorias_pratos';
        $result_cat = mysqli_query($link, $query_cat);
?>

        <div id="newEditDiv" class="col-md-9" "col-sm-9" "col-xs-9">

            <h2>Editar Dia</h2>

            <br>
            <br>

            <form action="" method="post">
                <input type="hidden" name="saved" value="1">
                <input type="hidden" name="id" value="<?= $id ?>">
                <div class="form-group">
                    <label for="selectPratos">Pratos</label>
                    <select name="pratos[]" id="selectPratos" class="form-control" multiple>
                        <?php
            
            while ($row_cat = mysqli_fetch_array($result_cat)) {

                    $query_pratos = 'SELECT DISTINCT pratos.id,
                                    CONCAT(descricao) AS prato,
                                    IF(pratos.id IN (
                                    SELECT pratos_id FROM ementa_has_pratos WHERE ementa_id = '. $id .'), TRUE, FALSE) 
                                    AS prato_dia 
                                    FROM ementa_has_pratos
                                    JOIN pratos ON pratos.id = ementa_has_pratos.pratos_id
                                    JOIN ementa ON ementa.id = ementa_has_pratos.ementa_id
                                    ORDER BY id ASC';
                
                    $result_pratos = mysqli_query($link, $query_pratos);
                        
                    while ($row_pratos = mysqli_fetch_array($result_pratos)) {
                        
                            $selected = ' ';
                            if ($row_pratos['prato_dia']) {
                                $selected = ' selected ';
                            }
                        
                            echo '<option value="'.$row_pratos['id'].'" '.$selected.'>';    
                
                            echo $row_pratos['prato'];
                
                            echo '</option>';
                        
                    }

                }    
            ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Submeter</button>

                <a class="btn btn-default" href="ementa.php" role="button">Voltar</a>

                <br>
                <br>

            </form>

        </div>
        <div style="clear: both"></div>
        <?php include('footer.php'); ?>