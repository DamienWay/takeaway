<?php include('header.php'); ?>

    <?php
    
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;

$saved = isset($_REQUEST['saved']);

if ($saved) { 
    
    $descricao = $_REQUEST['descricao'];
    
    if ($id == 0) { 
        
        $query = 'INSERT INTO categorias_pratos (descricao) VALUES(?)';
    
        $stmt = mysqli_prepare($link, $query);
        mysqli_stmt_bind_param($stmt, 's', $descricao);
        $success = mysqli_stmt_execute($stmt);
        
        if ($success) {
            $id = mysqli_stmt_insert_id($stmt);    
        }
        
        mysqli_stmt_close($stmt);
        
        echo '<div class="alert alert-success text-center" role="alert">Categoria Adicionada com sucesso</div>';
        
    } else { 
        
        $query = 'UPDATE categorias_pratos SET descricao = ? WHERE id = ?';
    
        $stmt = mysqli_prepare($link, $query);
        mysqli_stmt_bind_param($stmt, 's', $descricao);
        $success = mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        
        echo '<div class="alert alert-success text-center" role="alert">Categoria Editada com sucesso</div>';
 
    }
}
    

?>

        <?php
    
$query = 'SELECT * FROM categorias_pratos WHERE id = ' . $id;

$result = mysqli_query($link, $query);
$row = mysqli_fetch_array($result);

$descricao = $row['descricao'];


?>

            <div id="newEditDiv" class="col-md-9" "col-sm-9" "col-xs-9">
                <?php

if ($id == 0) {
    echo '<h2>Nova Categoria</h2>';
} else {
    echo '<h2>Editar Categoria</h2>';
}

?>

                    <br>
                    <br>

                    <form action="" method="post">
                        <input type="hidden" name="saved" value="1">
                        <input type="hidden" name="id" value="<?= $id ?>">
                        <div class="form-group">
                            <label for="inputDescricao">Descriçao</label>
                            <input type="text" class="form-control" id="inputDescricao" name="descricao" placeholder="Descricao" value="<?= $descricao ?>">
                        </div>


                        <button type="submit" class="btn btn-primary">Submeter</button>

                        <a class="btn btn-default" href="categorias_pratos.php" role="button">Voltar</a>
                        <br>
                        <br>
                    </form>

            </div>
            <!-- /.container -->
            <div style="clear: both"></div>
            <?php include('footer.php'); ?>