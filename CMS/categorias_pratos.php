<?php include('header.php') ?>

    <?php 

    $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;

    if ($sort) {
        
        $sortArray = explode(',', $sort);
        
        
        mysqli_query($link, 'START TRANSACTION');
        
        $sort_success = true;
        
        foreach($sortArray as $sort_order => $row_id) {
            $query_sort = "UPDATE categorias_pratos SET ordem = $sort_order WHERE id = $row_id";
            if (!mysqli_query($link, $query_sort)) {
                $sort_success = false;
                break;
            }            
        }
        
        
        if ($sort_success) {
            mysqli_query($link, 'COMMIT');    
        } else {
            mysqli_query($link, 'ROLLBACK'); 
        }   
    }

?>



        <div id="divTableEmenta" class="col-md-9" "col-sm-9" "col-xs-9">

            <br>

            <button id="btnSaveOrder" class="btn btn-default center-block" disabled>Guardar Nova Ordem</button>

            <br>

            <table id="tableGeral" class="table table-hover" class="align-center">


                <?php
    
        $query_cat = 'SELECT * FROM categorias_pratos ORDER BY ordem ASC';
        $result_cat = mysqli_query($link, $query_cat);

              echo '<tbody>'; 
            
                    while ($row_cat = mysqli_fetch_array($result_cat)) {

                        echo '<tr data-sort-element-id=' . $row_cat['id'] . '>';

                        echo '<td>' . $row_cat['descricao'] . '</td>';

                        
                        echo '<td><a class="btn btn-info btnMove">Mover</a><a class="btn btn-default" href="new_cat_pratos.php?id='.$row_cat['id'].'" role="button">Editar</a></td>';
                        
                        echo '</tr>'; 
                        
                    }
                    echo '</tbody>';
            
            
        ?>

            </table>

            <a id="newEditBtn" class="btn btn-primary" href="new_cat_pratos.php" role="button">Nova Categoria de Pratos</a>
            <br>
            <br>

        </div>
        <div style="clear: both"></div>
        <?php include('footer.php') ?>