<?php include('header.php'); ?>

<?php
    
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;

$saved = isset($_REQUEST['saved']);

if ($saved) { 
    
    $descricao = $_REQUEST['descricao'];
    $conteudo = $_REQUEST['conteudo'];
    
    if ($id == 0) { 
        
        $query = 'INSERT INTO contactos (descricao, conteudo) VALUES(?, ?)';
    
        $stmt = mysqli_prepare($link, $query);
        mysqli_stmt_bind_param($stmt, 'ss', $descricao, $conteudo);
        $success = mysqli_stmt_execute($stmt);
        
        if ($success) {
            $id = mysqli_stmt_insert_id($stmt);    
        }
        
        mysqli_stmt_close($stmt);
        
         echo '<div class="alert alert-success text-center" role="alert">Contacto Adicionado com sucesso</div>';
        
    } else { 
        
        $query = 'UPDATE contactos SET descricao = ?, conteudo = ? WHERE id = ?';
    
        $stmt = mysqli_prepare($link, $query);
        mysqli_stmt_bind_param($stmt, 'ssi', $descricao, $conteudo, $id);
        $success = mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        
         echo '<div class="alert alert-success text-center" role="alert">Editado com sucesso</div>';
        
    } 
    
    
}

?>

<?php
    
$query = 'SELECT * FROM contactos WHERE id = ' . $id;

$result = mysqli_query($link, $query);
$row = mysqli_fetch_array($result);

$descricao = $row['descricao'];
$conteudo = $row['conteudo']

?>

<div id="newEditDiv" class="col-md-9" "col-sm-9" "col-xs-9">  
<?php

if ($id == 0) {
    echo '<h2>Novo Contacto</h2>';
} else {
    echo '<h2>Editar Contacto</h2>';
}

?>
    
    <br>
    <br>
       
    <form action="" method="post">
        <input type="hidden" name="saved" value="1">
        <input type="hidden" name="id" value="<?= $id ?>">
        <div class="form-group">
            <label for="inputDescricao">Descriçao</label>
            <input type="text" class="form-control" id="inputDescricao" name="descricao" placeholder="Descricao" value="<?= $descricao ?>">
        </div> 
        <div class="form-group">
            <label for="inputConteudo">Conteudo</label>
            <input type="text" class="form-control" id="inputConteudo" name="conteudo" placeholder="Conteudo" value="<?= $conteudo ?>">
        </div>      
        
        <button type="submit" class="btn btn-primary">Submeter</button>
        
        <a class="btn btn-default" href="contactos.php" role="button">Voltar</a><br><br>
    </form>

</div>
<!-- /.container -->
 <div style="clear: both"></div>
<?php include('footer.php'); ?>
