<?php include('header.php') ?>

    <?php 
    
    $del_id = isset($_REQUEST['delId']) ? $_REQUEST['delId'] : false;
    $affected_rows = 0;
    if ($del_id) {
        
        $query = 'DELETE FROM bebidas_sobremesas WHERE id = ' . $del_id;
        $sucesso = mysqli_query($link, $query);
        
        if ($sucesso) {
            $affected_rows = mysqli_affected_rows($link);   
        }
    } 

    
    $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;

    if ($sort) {
        
        $sortArray = explode(',', $sort);
        
        
        mysqli_query($link, 'START TRANSACTION');
        
        $sort_success = true;
        
        foreach($sortArray as $sort_order => $row_id) {
            $query_sort = "UPDATE bebidas_sobremesas SET ordem = $sort_order WHERE id = $row_id";
            if (!mysqli_query($link, $query_sort)) {
                $sort_success = false;
                break;
            }            
        }
        
        
        if ($sort_success) {
            mysqli_query($link, 'COMMIT');    
        } else {
            mysqli_query($link, 'ROLLBACK'); 
        }   
    }

?>

        <?php
    
    if ($del_id) {
        if ($affected_rows > 0) {
            echo '<div class="alert alert-success text-center" role="alert">Apagado com sucesso</div>';

        } 
        
    }
    

?>


            <div id="divTableEmenta" class="col-md-9" "col-sm-9" "col-xs-9">

                <br>

                <button id="btnSaveOrder" class="btn btn-default center-block" disabled>Guardar Nova Ordem</button>

                <br>

                <table id="tableGeral" class="table table-hover" class="align-center">


                    <?php
    
        $query_cat = 'SELECT * FROM categorias_bebidas';
        $result_cat = mysqli_query($link, $query_cat);

            while ($row_cat = mysqli_fetch_array($result_cat)) {
                
                echo '<thead>';
                echo '<tr>';
                echo '<th class="text-center">' . $row_cat['descricao'] . '<th>';
                echo '</tr>';
                echo '</thead>';
            
                $query_bebidas = "SELECT bebidas_sobremesas.descricao, bebidas_sobremesas.id, preco 
                            FROM bebidas_sobremesas
                            JOIN categorias_bebidas ON categorias_bebidas.id = categorias_bebidas_id
                            WHERE categorias_bebidas_id =" . $row_cat['id'] . " ORDER by bebidas_sobremesas.ordem ASC";
             

                $result_bebidas = mysqli_query($link, $query_bebidas);
        
                    echo '<tbody>'; 
                    while ($row_bebidas = mysqli_fetch_array($result_bebidas)) {

                        
                        
                        echo '<tr data-sort-element-id=' . $row_bebidas['id'] . '>';

                        echo '<td>' . $row_bebidas['descricao'] . '</td>';

                        echo '<td>' . $row_bebidas['preco'] . '</td>';  
                        
                        echo '<td><a class="btn btn-info btnMove">Mover</a>  <button class="btn btn-danger button-del" type="button" data-del-id="'.$row_bebidas['id'].'">Apagar</button></td>';
                        
                        echo '</tr>'; 
                        
                    }
                    echo '</tbody>';
                }   
        ?>

                </table>

                <a id="newEditBtn" class="btn btn-primary" href="bebidas_detail.php" role="button">Nova Bebida/Sobremesa</a>
                <br>
                <br>

            </div>
            <div style="clear: both"></div>
            <?php include('footer.php') ?>