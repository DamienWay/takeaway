function dragHandler(event) {
            event.stopPropagation();
            event.preventDefault();
            event.dataTransfer.dropEffect = 'copy';

        }

        function dropHandler(event) {
            event.stopPropagation();
            event.preventDefault();


            addFiles(event.dataTransfer.files);

        }



        function addFiles(files) {
            for (var i = 0; i < files.length; i++) {

                var file = files[i];

                if (!file || !file.type.match(/image\/.*|application\/pdf/)) {
                    alert("Este ficheiro não é um pdf...");
                    return;
                }

                var reader = new FileReader();
                reader.onload = function (event) {

                    var dataUrl = event.target.result;

                    $('#divEmentaPDF').append('<object data="' + dataUrl + '" type="application/pdf">');

                }
                reader.readAsDataURL(file);
            }
            
        }


$(document).ready(function () {

    $('.button-del').click(function (e) {

        if (confirm('Quer mesmo apagar?')) {
            var delId = $(this).data('delId');

            window.location = '?delId=' + delId;
        }
    });
    
    
    

    $("#tableGeral tbody").sortable({
        
        placeholder: 'sortPlaceHolder',
        handle: '.btnMove',
        update: function (event, ui) {
            $('#btnSaveOrder').attr('disabled', false);
            $('#btnSaveOrder').removeClass('btn-default');
            $('#btnSaveOrder').addClass('btn-success');

           
        }
    });
    
    
    $("#tableGeral #tBodyEmenta").sortable({
        
        placeholder: 'sortPlaceHolder',
        handle: '.btnMove',
        update: function (event, ui) {
           

            $('.btnSaveOrderEmenta').attr('disabled', false);
            $('.btnSaveOrderEmenta').removeClass('btn-default');
            $('.btnSaveOrderEmenta').addClass('btn-success');
        }
    });
   
    

    $('#btnSaveOrder').click(function () {

        var options = {
            'attribute': 'data-sort-element-id'
        };

        var sortedArray = $("#tableGeral tbody").sortable("toArray", options);

        window.location = '?sort=' + sortedArray;

    });


    $('.btnSaveOrderEmenta').click(function () { 
        
      var sortId = $(this).data('diaId');
  
        
      var options = {
          'attribute': 'data-sort-element-id-' + sortId
      };

      var newSort = '';
      var lastEmpty = true;
      $("#tableGeral tbody").each(function () {
          if (!lastEmpty) {
              newSort += ',';
          }
          var sort = $(this).sortable("toArray", options);

          if (sort.length != 0 && sort != ',') {

              newSort += sort;

              lastEmpty = false;
          } else {
              lastEmpty = true;
          }
      });
        
       // var newSorted = newSort.replace(/[\s,]+/g,' ').trim();
        
        
        /[,\s]+|[,\s]+/g

    

        var newSorted = newSort.replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',');

        
        
        window.location = '?sort=' + newSorted + '&id=' + sortId;
        
  });


    var dropZone = $("#dropZone")[0];

        dropZone.addEventListener('dragover', dragHandler, true);
        dropZone.addEventListener('drop', dropHandler, true);
    
    
});