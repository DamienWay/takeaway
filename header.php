<?php include('connect.php') ?>

<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Take Away!</title>

    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    
    <link rel="stylesheet" type="text/css" href="css/my_styles.css">
    

    <script src="js/jquery-2.2.0.min.js"></script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCa2iP-_kXCr67T28n6UZJRUnSJW_xMf_I&libraries=visualization">
    </script>
    
    <script src="js/my_jscript.js"></script>
</head>

<body>

    <div id="mainWrapper">

        <div id="leftBannerDiv">
            <a href="index.php">
                <div id="linkBanner"></div>
            </a>
            <img src="imgs/mainRibbon.png" id="mainBanner">
            <ul>
                <li>
                    <h1><a href="index.php">Ementa</a></h1></li>
                <li>
                    <h4>diária</h4></li>
                <li>
                    <h1><a href="especialidades.php">Especialidades</a></h1></li>
                <li>
                    <h4>por encomenda</h4></li>
                <li>
                    <h1><a href="bebidas.php">Bebidas</a></h1></li>
                <li>
                    <h4>& sobremesas</h4></li>
                <li>
                    <h1><a href="contactos.php">Contactos</a></h1></li>
                <li>
                    <h4>horários e encomendas</h4></li>
            </ul>

            <p>Venha exprimentar a nossa Picanha ao Fim-de-Semana!</p>

        </div>