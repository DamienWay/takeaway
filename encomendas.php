<?php include('header.php') ?>

    <?php

    $saved = isset($_REQUEST['saved']);

if ($saved) { 
    
    $nome = $_REQUEST['nome'];
    $morada = $_REQUEST['morada'];
    $numero = $_REQUEST['numero'];
    
    if(!empty($_REQUEST['dosesEsp'])) {
        
        $doses_esp = $_REQUEST['dosesEsp'];
    }
    if(!empty($_REQUEST['doses'])) {
        
         $doses_pratos = $_REQUEST['doses'];
    }
   
    if(!empty($_REQUEST['quantidade'])) {
        
       $quantidade_sobr_beb = $_REQUEST['quantidade'];   
    }
   
    
    
    date_default_timezone_set('GMT');
    
    $dia_encomenda = date('Y.m.d');
        
    $success = true; 
    
        if(!empty($doses_pratos)) {
            
            $pratos_doses = array_combine((array)$_SESSION['encomenda_pratos'], (array)$doses_pratos);
            
            
            foreach((array)$_SESSION['encomenda_pratos'] as $pratos) {
                
                foreach((array)$doses_pratos as $doses) {
                    
                    $query_pratos = 'SELECT * FROM pratos WHERE id = ' . $pratos;
                    $result_pratos = mysqli_query($link, $query_pratos);

                    while ($row_pratos = mysqli_fetch_array($result_pratos)) {

                        $query_pratos = 'INSERT INTO encomendas (descricao, dose_quantidade, nome_cliente, morada, numero_cliente, dia) VALUES(?, ?, ?, ?, ?, ?)';

                        $stmt_pratos = mysqli_prepare($link, $query_pratos);
                        mysqli_stmt_bind_param($stmt_pratos, 'ssssss', $row_pratos['descricao'], $doses, $nome, $morada, $numero, $dia_encomenda);
                        mysqli_stmt_execute($stmt_pratos);
                        if(mysqli_stmt_execute($stmt_pratos)) {
                            
                            $success = true;
                        } else {
                            
                           $success = false; 
                        }
                    }
                    
                } 
                
                

            }  
            mysqli_stmt_close($stmt_pratos);
        }
    
        if(!empty($quantidade_sobr_beb)) {
           
            
            foreach((array)$_SESSION['encomenda_bebidas'] as $bebidas) {
                
                foreach((array)$quantidade_sobr_beb as $quantidade) {
                    
                    $query_bebidas = 'SELECT * FROM bebidas_sobremesas WHERE id = ' . $bebidas;
                    $result_bebidas = mysqli_query($link, $query_bebidas);

                    while ($row_bebidas = mysqli_fetch_array($result_bebidas)) {

                        $query_bebidas = 'INSERT INTO encomendas (descricao, dose_quantidade, nome_cliente, morada, numero_cliente, dia) VALUES(?, ?, ?, ?, ?, ?)';

                        $stmt_bebidas = mysqli_prepare($link, $query_bebidas);
                        mysqli_stmt_bind_param($stmt_bebidas, 'ssssss', $row_bebidas['descricao'], $quantidade, $nome, $morada, $numero, $dia_encomenda);
                        mysqli_stmt_execute($stmt_bebidas);
                        if(mysqli_stmt_execute($stmt_bebidas)) {
                            
                            $success = true;
                        } else {
                            
                            $success = false; 
                        }
                    }
                    
                }
                
                

            }  
           mysqli_stmt_close($stmt_bebidas); 
        }
    
        if(!empty($doses_esp)) {

            foreach((array)$_SESSION['encomenda_especialidades'] as $especialidades) {
                
                foreach((array)$doses_esp as $doses_espc) {
                    
                    $query_especialidades = 'SELECT * FROM especialidades WHERE id = ' . $especialidades;
                    $result_especialidades = mysqli_query($link, $query_especialidades);

                    while ($row_especialidades = mysqli_fetch_array($result_especialidades)) {

                        $query_esp = 'INSERT INTO encomendas (descricao, dose_quantidade, nome_cliente, morada, numero_cliente, dia) VALUES(?, ?, ?, ?, ?, ?)';

                        $stmt_esp = mysqli_prepare($link, $query_esp);
                        mysqli_stmt_bind_param($stmt_esp, 'ssssss', $row_especialidades['especialidades'], $doses_espc, $nome, $morada, $numero, $dia_encomenda);
                        mysqli_stmt_execute($stmt_esp);
                        if(mysqli_stmt_execute($stmt_esp)) {
                            
                            $success = true;
                        } else {
                            
                           $success = false; 
                        }
                        
                    }
                    
                }
                
               

            }  
         
            mysqli_stmt_close($stmt_esp);
        }
        
    
     if($success) {  
         
        mysqli_query($link, 'COMMIT');

            echo " <script type=\"text/javascript\">
                        alert('Encomenda pedida com sucesso. Obrigado pela escolha.')
                        window.location = 'index.php'; 
                    </script>";
        }
}


?>

        <div id="rightEmentaDiv">

            <form action="" method="post" id="formEncomendas">
                <input type="hidden" name="saved" value="1">

                <?php 

                        if(!empty($_SESSION['encomenda_pratos'])) {
                            
                            echo '<h3>Ementa para Encomenda</h3>';

                            foreach((array) $_SESSION['encomenda_pratos'] as  $key=>$item){
                               
                                $query_pratos = 'SELECT * FROM pratos WHERE id = ' . $item;
                                $result_pratos = mysqli_query($link, $query_pratos);

                                while ($row_pratos = mysqli_fetch_array($result_pratos)) {

                                    echo $row_pratos ['descricao'] . '   <div class>
                                                <label for="inputDoses">Doses</label>
                                    <input type="number" id="inputDoses" name="doses" placeholder="Doses" step="0.5" value="0">  <a id="btnMais" role="button">+</a>     <a id="btnMenos" role="button">-</a> </div><br>';

                                  

                                }
                            }

                        }


                        if(!empty($_SESSION['encomenda_bebidas'])) {

                             echo '<h3>Bebidas e Sobremesas para Encomenda</h3>';
                            
                            foreach((array) $_SESSION['encomenda_bebidas'] as $bebidas){

                                $query_bebidas = 'SELECT * FROM bebidas_sobremesas WHERE id = ' . $bebidas;
                                $result_bebidas = mysqli_query($link, $query_bebidas);

                                while ($row_bebidas = mysqli_fetch_array($result_bebidas)) {
                                  
                                    echo  $row_bebidas ['descricao'] ;

                                    echo '<div>
                                                <label for="inputQuant">Quantidade</label>
                                    <input type="number" id="inputQuant" name="quantidade" placeholder="Quantidade" value="0">  <a id="btnMaisBeb" role="button">+</a>     <a id="btnMenosBeb" role="button">-</a> </div><br>';

                                

                                }
                            }

                        }


                        if(!empty($_SESSION['encomenda_especialidades'])) {

                              echo '<h3>Especialidades para Encomenda</h3>';

                            
                             foreach((array) $_SESSION['encomenda_especialidades'] as $especialidades){

                                $query_especialidades = 'SELECT * FROM especialidades WHERE id = ' . $especialidades;
                                $result_especialidades = mysqli_query($link, $query_especialidades);

                                while ($row_especialidades = mysqli_fetch_array($result_especialidades)) {

                                   
                                    echo $row_especialidades ['especialidades'];

                                    echo '<div>
                                                <label for="inputDosesEsp">Doses</label>
                                    <input type="number" id="inputDosesEsp" name="dosesEsp" placeholder="Doses" step="0.5" value="0">  <a id="btnMais" role="button">+</a>     <a id="btnMenos" role="button">-</a> </div> <br>';

                    

                                }
                            }

                        }

                    ?>

                    <h3>Dados Adicionais</h3>
                    <div>
                        <label for="inputNome">Nome</label>
                        <input type="text" id="inputNome" name="nome" placeholder="Nome">
                    </div>
                    <br>
                    <div>
                        <label for="inputMorada">Morada</label>
                        <input type="text" id="inputMorada" name="morada" placeholder="Morada">
                    </div>
                    <br>
                    <div>
                        <label for="inputNumero">Número de Telefone</label>
                        <input type="text" id="inputNumero" name="numero" placeholder="Número de Telefone">
                    </div>
                    <br>

                    <button id="btnFinal" type="submit">Submeter</button> <br><br> 
                    <a id="btnFinal" href="index.php" role="button">Voltar</a>
            </form>
                    



        </div>


        <?php include('footer.php') ?>