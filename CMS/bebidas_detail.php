<?php include('header.php'); ?>

    <?php
    
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;

$saved = isset($_REQUEST['saved']);

if ($saved) { 
    
    $descricao = $_REQUEST['descricao'];
    $preco = $_REQUEST['preco'];
    $cat_id = $_REQUEST['cat_id'];
    
    if ($id == 0) { // NOVO
        
        $query = 'INSERT INTO bebidas_sobremesas (descricao, preco, categorias_bebidas_id) VALUES(?, ?, ?)';
    
        $stmt = mysqli_prepare($link, $query);
        mysqli_stmt_bind_param($stmt, 'ssi', $descricao, $preco, $cat_id);
        $success = mysqli_stmt_execute($stmt);
        
        if ($success) {
            $id = mysqli_stmt_insert_id($stmt);    
        }
        
        mysqli_stmt_close($stmt);
        
    } 
    
   if ($success) {
        mysqli_query($link, 'COMMIT');
        echo ' <div class="alert alert-success text-center" role="alert">Bebida/Sobremesa adicionada com sucesso.</div>';
    } else {
        mysqli_query($link, 'ROLLBACK');
        echo '<div class="alert alert-danger text-center" role="alert">Bebida/Sobremesa não adicionada...</div>';
    }
}

?>


        <div id="newEditDiv" class="col-md-9" "col-sm-9" "col-xs-9">

            <h2>Nova Bebida/Sobremesa</h2>

            <br>
            <br>

            <form action="" method="post">
                <input type="hidden" name="saved" value="1">
                <div class="form-group">
                    <label for="inputDescricao">Descrição da Bebida/Sobremesa</label>
                    <input type="text" class="form-control" id="inputDescricao" name="descricao" placeholder="Descriçao da Bebida/Sobremesa">
                </div>

                <div class="form-group">
                    <label for="inputPreco">Preço</label>
                    <input type="text" class="form-control" id="inputPreco" name="preco" placeholder="Preço">
                </div>

                <div class="form-group">
                    <label for="selectCat">Categoria</label>
                    <select class="form-control" id="selectCat" name="cat_id">
                        <?php
            
            $query_cat = 'SELECT * FROM categorias_bebidas';
            
            $result_cat = mysqli_query($link, $query_cat);
            while ($row_cat = mysqli_fetch_array($result_cat)) {
                
                echo '<option value="'.$row_cat['id'].'">';    
                
                echo $row_cat['descricao'];
                echo '</option>';
                
            }
            ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Submeter</button>

                <a class="btn btn-default" href="bebidas.php" role="button">Voltar</a>

                <br>
                <br>

            </form>

        </div>
        <div style="clear: both"></div>
        <?php include('footer.php'); ?>