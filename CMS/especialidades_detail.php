<?php include('header.php'); ?>

    <?php
    
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;

$saved = isset($_REQUEST['saved']);

if ($saved) { 
    
    $especialidade = $_REQUEST['especialidade'];
    $preco = $_REQUEST['preco'];
    
    if ($id == 0) { 
        
        $query = 'INSERT INTO especialidades (especialidades, preco) VALUES(?, ?)';
    
        $stmt = mysqli_prepare($link, $query);
        mysqli_stmt_bind_param($stmt, 'ss', $especialidade, $preco);
        $success = mysqli_stmt_execute($stmt);
        
        if ($success) {
            $id = mysqli_stmt_insert_id($stmt);    
        }
        
        mysqli_stmt_close($stmt);
        
    } 
    
    if ($success) {
        mysqli_query($link, 'COMMIT');
        echo ' <div class="alert alert-success text-center" role="alert">Especialidade adicionada com sucesso.</div>';
    } else {
        mysqli_query($link, 'ROLLBACK');
        echo '<div class="alert alert-danger text-center" role="alert">Especialidade não adicionada...</div>';
    }
}


?>

        <div id="newEditDiv" class="col-md-9" "col-sm-9" "col-xs-9">

            <?php

    echo '<h2>Nova Especialidade</h2>';

?>

                <br>
                <br>

                <form action="" method="post">
                    <input type="hidden" name="saved" value="1">
                    <div class="form-group">
                        <label for="inputEspecialidade">Especialidade</label>
                        <input type="text" class="form-control" id="inputEspecialidade" name="especialidade" placeholder="Especialidade">
                    </div>
                    <div class="form-group">
                        <label for="inputPreco">Preço</label>
                        <input type="text" class="form-control" id="inputPreco" name="preco" placeholder="Preço">
                    </div>

                    <button type="submit" class="btn btn-primary">Submeter</button>

                    <a class="btn btn-default" href="especialidades.php" role="button">Voltar</a>
                    <br>
                    <br>
                </form>

        </div>
        <!-- /.container -->
        <div style="clear: both"></div>
        <?php include('footer.php'); ?>