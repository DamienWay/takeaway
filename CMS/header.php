<?php
    session_start();

    if (!isset($_SESSION['is_logged'])) {
        header('Location: login.php');
    }
?>

    <?php include('connect.php') ?>


        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>Takeaway Dashboard</title>

            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

            <link rel="stylesheet" href="css/my_css.css">

            <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

            <link rel="stylesheet" href="css/bootstrap.min.css">

            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

            <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

            <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">

            <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

            <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

        </head>

        <body class="hold-transition skin-blue sidebar-mini">
            <div class="wrapper">

                <header class="main-header">

                    <!-- Logo -->
                    <a href="index.php" class="logo">
                        <!-- mini logo for sidebar mini 50x50 pixels -->
                        <span class="logo-mini"><b>T</b>AY</span>
                        <!-- logo for regular state and mobile devices -->
                        <span class="logo-lg"><b>Takeaway!</b></span>
                    </a>

                    <!-- Header Navbar: style can be found in header.less -->
                    <nav class="navbar navbar-static-top" role="navigation">

                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- Messages: style can be found in dropdown.less-->
                                <li class="dropdown messages-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="label label-success">0</span>
                                    </a>
                                    <li class="dropdown user user-menu">
                                        <a href="contactos.php">
                                            <span class="hidden-xs">Administrador</span>
                                        </a>
                                    </li>
                            </ul>
                        </div>
                    </nav>
                </header>
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="main-sidebar">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <ul class="sidebar-menu">
                            <li class="header">Navegação das Páginas</li>
                            <li class="treeview">
                                <a href="index.php">
                                    <span class="glyphicon glyphicon-home"></span>
                                    <span>Index</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="../index.php">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                    <span>Site</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="ementa.php">
                                    <span class="glyphicon glyphicon-cutlery"></span>
                                    <span>Ementa</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="categorias_pratos.php">
                                    <span class="glyphicon glyphicon-cutlery"></span>
                                    <span>Categorias dos Pratos</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="bebidas.php">
                                    <span class="glyphicon glyphicon-glass"></span>
                                    <span>Bebidas e Sobremesas</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="categorias_bebidas.php">
                                    <span class="glyphicon glyphicon-glass"></span>
                                    <span>Categorias Bebidas/Sobremesas</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="especialidades.php">
                                    <span class="glyphicon glyphicon-thumbs-up"></span>
                                    <span>Especialidades</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="contactos.php">
                                    <span class="glyphicon glyphicon-book"></span>
                                    <span>Contactos</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="login.php?logout=1">
                                    <span class="glyphicon glyphicon-off"></span>
                                    <span>Logout</span>
                                </a>
                            </li>

                        </ul>
                    </section>
                </aside>
            </div>