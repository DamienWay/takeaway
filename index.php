<?php include('header.php') ?>

    <div id="rightEmentaDiv">

        <?php
    
        $diaSemana = date('w');

                    if ($diaSemana == 0) {
                        
                        $diaSemana = 7;
                    }
    
        $encomenda_pratos = explode(",", isset($_REQUEST['encomenda']) ? $_REQUEST['encomenda'] : 0);
     

        $_SESSION['encomenda_pratos'] = $encomenda_pratos;


        $query_dia = 'SELECT * FROM ementa';
        $result_dia = mysqli_query($link, $query_dia);

        while ($row_dia = mysqli_fetch_array($result_dia)) {
            
            echo    '<img src="imgs/backLabel.png">';
            echo    '<h4>'. $row_dia['dia'] .'</h4>';
            echo    '<div class="divEscondida">';
            echo    '<table>';
            
            $query_cat = 'SELECT * FROM categorias_pratos';
            $result_cat = mysqli_query($link, $query_cat);
            
            while ($row_cat = mysqli_fetch_array($result_cat)) {
            
                
            $query_pratos = 'SELECT pratos.id, descricao, dose, `meia-dose`
                            FROM ementa_has_pratos
                            JOIN pratos ON pratos.id = ementa_has_pratos.pratos_id
                            JOIN ementa ON ementa.id = ementa_has_pratos.ementa_id
                            WHERE ementa.id =' . $row_dia['id'] .
                            ' AND pratos.categorias_pratos_id =' . $row_cat['id'];
                
            
            $result_pratos = mysqli_query($link, $query_pratos);
            $num_rows = mysqli_num_rows($result_pratos);     
            
            if(!empty($num_rows)) {
                echo '<tr><th>' . $row_cat['descricao'] . '</th><td>Dose</td><td>&frac12 Dose</td></tr>';
                
                while ($row_pratos = mysqli_fetch_array($result_pratos)) {
                
                    
                        echo '<tr>';
                    
                        if ($row_dia['id'] == $diaSemana) {
                            
                            echo '<td><input type="checkbox" id="checkEncomenda" name="pratos" value="'.$row_pratos['id'].'">' . $row_pratos['descricao'] . '</td>';
                
                            echo '<td>' . $row_pratos['dose'] . '</td>';

                            echo '<td>' . $row_pratos['meia-dose'] . '</td>';  
                            
                        } else {
                            
                            echo '<td>' . $row_pratos['descricao'] . '</td>';
                
                            echo '<td>' . $row_pratos['dose'] . '</td>';

                            echo '<td>' . $row_pratos['meia-dose'] . '</td>';    
                            
                        }

                        echo '</tr>'; 

                }

            }
            
        }
            
        echo '<tr><td><button type="button" class="buttonEncomendar">Juntar</button></td></tr>';        
        echo '</table></div>';
    }

        ?>
            <a class="btnJuntarEncomenda" href="encomendas.php" role="button">Encomendar</a>
            <span>*iva incluído</span>
    </div>
    <div class="clear"></div>

    <div id="footerDiv">
        <p>Projecto Web desenvolvido por <a href="#">Malta Fixe</a></p>

        <div id="newsletterDiv"></div>
        <form>
            <input type="text" id="newsletter" value="Subscrever Newsletter">
            <button type="submit" form="newsletter" value="Submit" id="buttonEnviar">Enviar</button>
        </form>
    </div>