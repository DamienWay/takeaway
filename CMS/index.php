<?php include('header.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Dashboard
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Likes</span>
                            <span class="info-box-number">2</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Vendas</span>
                            <span class="info-box-number">
                      
                      <?php
                            $query_vendas = "SELECT MAX(id) AS id FROM encomendas";
                            $result_vendas = mysqli_query($link, $query_vendas);

                            while($row_vendas = mysqli_fetch_array($result_vendas)) {
                                
                               echo $row_vendas['id']; 
                                
                            }
      
                      ?>
                    
                  </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-8">
                    <!-- TABLE: LATEST ORDERS -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Últimos Pedidos</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                        <tr>
                                            <th>Descriçao</th>
                                            <th>Doses/Quantidade</th>
                                            <th>Cliente</th>
                                            <th>Morada</th>
                                            <th>Número</th>
                                            <th>Dia</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php 
    
                        $query_enc = 'SELECT * FROM encomendas';
                        $result_enc = mysqli_query($link, $query_enc);

                        while ($row_enc = mysqli_fetch_array($result_enc)) {
                            echo '<tr>';
                            echo '<td>' .$row_enc['descricao'] . '</td>';
                            echo '<td>' .$row_enc['dose_quantidade']. '</td>';
                            echo '<td>' .$row_enc['nome_cliente']. '</td>';
                            echo '<td>' .$row_enc['morada']. '</td>';
                            echo '<td>' .$row_enc['numero_cliente']. '</td>';
                            echo '<td>' .$row_enc['dia']. '</td>';
                            echo ' </tr>';
                        }
        ?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">

                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->

                <div class="col-md-4">
                    <!-- Info Boxes Style 2 -->
                    <div class="box box-default">
                        <div class="box-footer no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#"><h4 class="box-title">Zona de Pedidos</h4></a></li>
                                <li><a href="#">Porto<span class="pull-right">75%</span></a></li>
                                <li><a href="#">Braga<span class="pull-right"> 20%</span></a></li>
                                <li><a href="#">Lisboa <span class="pull-right"> 3%</span></a></li>
                                <li><a href="#">Coimbra <span class="pull-right"> 2%</span></a></li>
                            </ul>
                        </div>
                        <!-- /.footer -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include('footer.php') ?>