function displayMap() {
    document.getElementById('direcoes').style.display = "block";
    initialize();
}

function initialize() {
    // create the map

    var myOptions = {
        zoom: 14,
        center: new google.maps.LatLng(41.1500494, -8.614952, 17),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("direcoes"),
        myOptions);

}


$(document).ready(function () {


    var hoje = new Date().getDay();
    var dias = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"][(new Date()).getDay()];



    if (hoje == 0) {

        hoje = 7
    }


    $('h4:nth-of-type(' + hoje + ')').next().show();


    $('h4').click(function () {

        if (!$(this).next().is(":visible")) {
            $(".divEscondida").slideUp();
            $(this).next().slideDown();
        }


    });

    $("#newsletter").focusin(function () {
        this.value = "";
    });

    $('#newsletter').focusout(function () {
        this.value = "Subscrever Newsletter";
    });

    $('.buttonEncomendar').click(function () {
        
        var encomenda = new Array();


        $("#checkEncomenda:checked").each(function() {
            encomenda.push($(this).val());
        });

           window.location = '?encomenda=' + encomenda;
           
    }); 
    
  

    $('#btnMais').click(function() {
        
        var valEmenta = $(this).prev().val();

        valEmenta = parseFloat(valEmenta) + 0;
        
        var valEmenta = $(this).prev().val(valEmenta);
        
        console.log(valEmenta);
    });
    
    $('#btnMenos').click(function() {
        
        var valEmenta = $(this).siblings('#inputDoses').val();

        valEmenta = parseFloat(valEmenta) - 0.5;
        
        var valEmenta = $(this).siblings('#inputDoses').val(valEmenta);
        
    });
    
      $('#btnMais').click(function() {
        
        var valEsp = $(this).prev().val();

        valEsp = parseFloat(valEsp) + 0.5;
        
        var valEsp = $(this).prev().val(valEsp);
        
    });
    
    $('#btnMenos').click(function() {
        
        var valEsp = $(this).siblings('#inputDosesEsp').val();

        valEsp = parseFloat(valEsp) - 0.5;
        
        var valEsp = $(this).siblings('#inputDosesEsp').val(valEsp);
        
    });
    
    $('#btnMaisBeb').click(function() {
        
        var valBeb = $(this).prev().val();

        valBeb = parseInt(valBeb) + 1;
        
        var valBeb = $(this).prev().val(valBeb);
        
    });
    
    $('#btnMenosBeb').click(function() {
        
        var valBeb = $(this).siblings('#inputQuant').val();

        valBeb = parseInt(valBeb) - 1;
        
        var valBeb = $(this).siblings('#inputQuant').val(valBeb);
        
    });
    
});





    