<?php include('header.php') ?>


    <?php   

    $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
    $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : false;

    if ($sort) {
        
        $sortArray = explode(',', $sort);
     
        mysqli_query($link, 'START TRANSACTION');
        
        $sort_success = true;
        
        // $key é a pos do array. Ex: 0, 1, 2 ...
        // $value é o id do prato
       foreach($sortArray as $key => $value) {
     //       $query_sort = "UPDATE ementa_has_pratos SET pratos_id =" .$sort_order. " WHERE ementa_id =" .$id;
    
           $query_sort = "UPDATE ementa_has_pratos SET ordem = $key WHERE ementa_id = $id AND pratos_id = $value";
            if (!mysqli_query($link, $query_sort)) {
                $sort_success = false;
                break;
            }            
        }
        
        
        if ($sort_success) {
            mysqli_query($link, 'COMMIT');    
        } else {
            mysqli_query($link, 'ROLLBACK'); 
        }   
    }



?>

        <div id="contentDiv" class="col-md-9" "col-sm-9" "col-xs-9">

            <br>

            <br>

            <table id="tableGeral" class="table table-hover" class="align-center">

                <?php
    
        $query_dia = 'SELECT * FROM ementa';
        $result_dia = mysqli_query($link, $query_dia);

        

            while ($row_dia = mysqli_fetch_array($result_dia)) {
                
                echo '<thead>';    
                echo '<tr>';
                echo '<th class="text-center">' . $row_dia['dia'] . '<th>';
                echo '<td><button  data-dia-id="' . $row_dia['id'] . '" class="btn btn-default center-block btnSaveOrderEmenta" disabled>Guardar Nova Ordem de '. $row_dia['dia'] .'</button> </td>';
                echo '<td><a class="btn btn-default" href="ementa_dia_detail.php?id='.$row_dia['id'].'" role="button"> Editar Dia</a></td>';
                echo '</tr>';
                echo '</thead>';    
                
                $query_cat = 'SELECT * FROM categorias_pratos';
                $result_cat = mysqli_query($link, $query_cat);
                
                while ($row_cat = mysqli_fetch_array($result_cat)) {
                    
                    echo '<thead>'; 
                    echo '<tr><td class="info" "align-center">' . $row_cat['descricao'] . '</td><td class="info">Dose</td><td class="info" "align-center">&frac12 Dose</td></tr>';
                    
                    echo '</thead>';

                    $query_pratos = 'SELECT ordem, pratos_id AS id_prato, pratos.id, descricao, dose, `meia-dose`
                            FROM ementa_has_pratos
                            JOIN pratos ON pratos.id = ementa_has_pratos.pratos_id
                            JOIN ementa ON ementa.id = ementa_has_pratos.ementa_id
                            WHERE ementa.id =' . $row_dia['id'] .
                            ' AND pratos.categorias_pratos_id =' . $row_cat['id'] .' ORDER BY ementa_has_pratos.ordem ASC';


                    $result_pratos = mysqli_query($link, $query_pratos);
                    $num_rows = mysqli_num_rows($result_pratos); 
                    
                    echo '<tbody id="tBodyEmenta">';
                        
                    while ($row_pratos = mysqli_fetch_array($result_pratos)) {
                        
                            //echo '<tr data-sort-dia-id="' . $row_dia['id'] . '">';
                        
                            echo '<tr data-sort-element-id-'. $row_dia['id'] . '="' . $row_pratos['id'] . '">';

                            echo '<td>' . $row_pratos['descricao'] . '</td>';

                            echo '<td>' . $row_pratos['dose'] . '</td>';

                            echo '<td>' . $row_pratos['meia-dose'] . '</td>';
                        
                            echo '<td><a class="btn btn-info btnMove">Mover</a></td>';
                            echo '</tr>';   
                        
                    }

                    echo '</tbody>'; 

                }    
                
                
            }

            

        ?>

            </table>
        </div>

        <div id="dropZone">
        Largue a ementa do dia aqui<br><br>
        
            <div id="divEmentaPDF"></div>
        </div>
        
       
        <div style="clear: both"></div>

        <?php include('footer.php') ?>