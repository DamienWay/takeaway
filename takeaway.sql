/*
SQLyog Community v11.51 (64 bit)
MySQL - 5.5.41-log : Database - takeaway_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`takeaway_db` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `takeaway_db`;

/*Table structure for table `administrador` */

DROP TABLE IF EXISTS `administrador`;

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `administrador` */

insert  into `administrador`(`id`,`user`,`password`) values (1,'admin@mail.com','admin');

/*Table structure for table `bebidas_sobremesas` */

DROP TABLE IF EXISTS `bebidas_sobremesas`;

CREATE TABLE `bebidas_sobremesas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `preco` varchar(255) NOT NULL,
  `categorias_bebidas_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bebidas_sobremesas_categorias_bebidas1_idx` (`categorias_bebidas_id`),
  CONSTRAINT `fk_bebidas_sobremesas_categorias_bebidas1` FOREIGN KEY (`categorias_bebidas_id`) REFERENCES `categorias_bebidas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `bebidas_sobremesas` */

insert  into `bebidas_sobremesas`(`id`,`descricao`,`preco`,`categorias_bebidas_id`,`ordem`) values (1,'Água Luso 1,5L','1,65€',1,1),(2,'Frissumo Ananás 1,5L','1,65€',1,2),(3,'Frissumo Laranja 1,5L','1,65€',1,0),(4,'Fanta Laranja 1L','1,65€',1,3),(5,'Seven-UP 2L','2,50€',1,4),(8,'Vinho Branco da Casa','6€',2,6),(9,'Vinho Verde da Casa','4€',2,7),(10,'Vinho Verde Branco Gazela','5€',2,8),(11,'Vinho Tinto da Casa','6€',2,9),(12,'Vinho Anima Magnum Tinto','9€',2,10),(13,'Vinho Altano Tinto','9€',2,11),(14,'Mousse de Chocolate','1,5€',3,12),(15,'Baba de Camela','2€',3,13),(16,'Molotof','1,5€',3,14),(17,'Pudim','1,5€',3,15),(18,'Tarte de Limão','1,5€',3,16),(19,'Strudel de Maça','2€',3,17),(20,'Crumble de Pêra','2€',3,18),(21,'Bolo de Bolacha','2€',3,19),(23,'Coca Cola 1L','2,5€',1,5);

/*Table structure for table `categorias_bebidas` */

DROP TABLE IF EXISTS `categorias_bebidas`;

CREATE TABLE `categorias_bebidas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `categorias_bebidas` */

insert  into `categorias_bebidas`(`id`,`descricao`,`ordem`) values (1,'Refrigerantes',1),(2,'Vinhos',0),(3,'Sobremesas',2);

/*Table structure for table `categorias_pratos` */

DROP TABLE IF EXISTS `categorias_pratos`;

CREATE TABLE `categorias_pratos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `categorias_pratos` */

insert  into `categorias_pratos`(`id`,`descricao`,`ordem`) values (1,'Entradas',0),(2,'Carne',2),(3,'Peixe',1),(4,'Vegetariano',3);

/*Table structure for table `contactos` */

DROP TABLE IF EXISTS `contactos`;

CREATE TABLE `contactos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `conteudo` text NOT NULL,
  `ordem` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `contactos` */

insert  into `contactos`(`id`,`descricao`,`conteudo`,`ordem`) values (1,'Morada','Rua São Pedro da Covas',1),(2,'Código Postal','4435-623',2),(3,'Telefone','224593584',3),(4,'Facebook','www.facebook.com/takeaway!',4),(5,'Distrito','Porto',5),(6,'Quem Somos','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent blandit vel lacus sit amet scelerisque. Cras aliquam leo turpis, id sagittis justo eleifend ut. Donec molestie tincidunt eros eu dictum. Suspendisse magna neque, luctus eget semper at, dictum nec quam. Pellentesque sit amet nisi facilisis ante pharetra aliquam vitae a elit. Etiam in dolor neque. Nullam euismod velit vel elit interdum pretium. Integer porttitor ullamcorper nulla quis dapibus. Etiam posuere, magna ac dignissim condimentum, neque massa ullamcorper leo, eu consequat odio tellus in orci. Donec blandit mauris in tortor pellentesque consequat.\r\n\r\nVestibulum ac placerat nisi. Cras vitae viverra lacus. Maecenas tincidunt ut tellus a convallis. Proin euismod imperdiet metus, et fringilla purus semper id. Sed et urna eu justo tempus lobortis ac sed lectus. Suspendisse interdum est arcu, sit amet dictum urna fermentum blandit. Ut tempor, nisl vitae egestas dictum, leo nisl ornare quam, sed molestie lacus libero id lectus. Suspendisse cursus nisl orci, sit amet dapibus lectus laoreet ut. Proin vitae venenatis justo, ut pretium dui. Praesent cursus aliquam odio et consequat. Duis ac lacinia nisi. Proin iaculis commodo aliquet. Mauris in lectus vel lectus auctor laoreet.\r\n\r\n',6),(7,'E-mail','admin@mail.com',0);

/*Table structure for table `ementa` */

DROP TABLE IF EXISTS `ementa`;

CREATE TABLE `ementa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dia` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `ementa` */

insert  into `ementa`(`id`,`dia`) values (1,'Segunda'),(2,'Terça'),(3,'Quarta'),(4,'Quinta'),(5,'Sexta'),(6,'Sábado'),(7,'Domingo');

/*Table structure for table `ementa_has_pratos` */

DROP TABLE IF EXISTS `ementa_has_pratos`;

CREATE TABLE `ementa_has_pratos` (
  `ementa_id` int(11) NOT NULL,
  `pratos_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL,
  PRIMARY KEY (`ementa_id`,`pratos_id`),
  KEY `fk_ementa_has_pratos_pratos1_idx` (`pratos_id`),
  KEY `fk_ementa_has_pratos_ementa_idx` (`ementa_id`),
  CONSTRAINT `fk_ementa_has_pratos_ementa` FOREIGN KEY (`ementa_id`) REFERENCES `ementa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ementa_has_pratos_pratos1` FOREIGN KEY (`pratos_id`) REFERENCES `pratos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ementa_has_pratos` */

insert  into `ementa_has_pratos`(`ementa_id`,`pratos_id`,`ordem`) values (1,1,0),(1,2,1),(1,10,3),(1,11,2),(1,28,4),(2,1,0),(2,4,1),(2,10,3),(2,11,2),(2,23,5),(2,24,4),(3,1,12),(3,12,13),(3,13,14),(3,25,15),(3,26,16),(4,1,1),(4,5,1),(4,14,1),(4,15,1),(4,27,1),(4,28,1),(5,1,1),(5,6,0),(5,16,2),(5,17,3),(5,29,4),(5,30,5),(6,1,29),(6,2,30),(6,18,31),(6,19,32),(6,31,33),(7,1,34),(7,7,35),(7,20,36),(7,21,37),(7,30,38),(7,32,38);

/*Table structure for table `encomendas` */

DROP TABLE IF EXISTS `encomendas`;

CREATE TABLE `encomendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `dose_quantidade` varchar(255) NOT NULL,
  `nome_cliente` varchar(255) NOT NULL,
  `morada` varchar(255) NOT NULL,
  `numero_cliente` varchar(9) NOT NULL,
  `dia` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;

/*Data for the table `encomendas` */

insert  into `encomendas`(`id`,`descricao`,`dose_quantidade`,`nome_cliente`,`morada`,`numero_cliente`,`dia`) values (1,'Arroz de Marisco','2','Joao Moreira','Rua do Castro Nº21 Leça da Palmeira','915784398','2016-06-01'),(134,'Sopa de Legumes','2','Diogo Machado','Rua do Já Foste','','2016.05.20'),(135,'Francesinha Especial c/Batatas','2','Diogo Machado','Rua do Já Foste','','2016.05.20'),(136,'Água Luso 1,5L','2','Diogo Machado','Rua do Já Foste','','2016.05.20'),(137,'Mousse de Chocolate','2','Diogo Machado','Rua do Já Foste','','2016.05.20'),(138,'Bacalhau António(2pax)','1','Diogo Machado','Rua do Já Foste','','2016.05.20'),(139,'Polvo assado no forno(2pax)','1','Diogo Machado','Rua do Já Foste','','2016.05.20');

/*Table structure for table `especialidades` */

DROP TABLE IF EXISTS `especialidades`;

CREATE TABLE `especialidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `especialidades` varchar(255) NOT NULL,
  `preco` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `especialidades` */

insert  into `especialidades`(`id`,`especialidades`,`preco`,`ordem`) values (3,'Bacalhau António(2pax)','21,00€',3),(4,'Polvo assado no forno(2pax)','22,00€',0),(5,'Polvo à lagareiro(2pax)','22,00€',1),(6,'Filetes de polvo(2pax)','22,00€',2),(7,'Pataniscas de bacalhau c/gambas(2pax)','11,00€',4),(8,'Arroz de tamboril c/gambas(2pax)','24,00€',6),(9,'Cabritinho assado no forno(2pax)**','24,00€',5);

/*Table structure for table `pratos` */

DROP TABLE IF EXISTS `pratos`;

CREATE TABLE `pratos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `dose` varchar(255) NOT NULL,
  `meia-dose` varchar(255) NOT NULL,
  `categorias_pratos_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pratos_categorias_pratos1_idx` (`categorias_pratos_id`),
  CONSTRAINT `fk_pratos_categorias_pratos1` FOREIGN KEY (`categorias_pratos_id`) REFERENCES `categorias_pratos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `pratos` */

insert  into `pratos`(`id`,`descricao`,`dose`,`meia-dose`,`categorias_pratos_id`) values (1,'Sopa de Legumes','1,50€','',1),(2,'Canja','2€','',1),(3,'Sopa de Nabos','1,5€','',1),(4,'Sopa Branca','1€','',1),(5,'Creme de Ervilha','2€','',1),(6,'Creme de Cenoura','2€','',1),(7,'Papas de Sarrabulho','4€','',1),(8,'Panados de Perú','6€','3€',2),(9,'Almôndegas','5,5€','2€',2),(10,'Vitela Assada','8€','4€',2),(11,'Strogonoff com Batata e Arroz','6€','3€',2),(12,'Lombo Assado no Forno','8€','5€',2),(13,'Costelinha na Brasa','11€','5€',2),(14,'Secretos de Porco Preto','11,5€','7€',2),(15,'Panados de Frango','5,5€','2€',2),(16,'Francesinha Especial c/Batatas','10€','',2),(17,'Massa à Bolonhesa','9€','4€',2),(18,'Frango Assado no Churrasco','12€','6€',2),(19,'Picanha Assada','14€','7€',2),(20,'Leitão Assado','16€','8€',2),(21,'Arroz de Cabidela','12€','6€',2),(22,'Pescada Estufada','7€','4€',3),(23,'Carapau Assado','7€','4€',3),(24,'Bacalhau com Natas','10€','5€',3),(25,'Red Fish na Brasa','8€','4€',3),(26,'Arroz de Tamboril','12€','6€',3),(27,'Lulas Recheadas','7€','4€',3),(28,'Filetes de Pescada','8€','4€',3),(29,'Arroz de Marisco','12€','6€',3),(30,'Bacalhau Assado na Brasa','12€','6€',3),(31,'Polvo à Lagareiro','12€','6€',3),(32,'Sardinha Assada','8€','4€',3),(33,'TOFU','10€','',4);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
