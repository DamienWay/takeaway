<?php include('header.php') ?>

    <?php 
    
    $del_id = isset($_REQUEST['delId']) ? $_REQUEST['delId'] : false;
    $affected_rows = 0;
    if ($del_id) {
        
        $query = 'DELETE FROM especialidades WHERE id = ' . $del_id;
        $sucesso = mysqli_query($link, $query);
        
        if ($sucesso) {
            $affected_rows = mysqli_affected_rows($link);   
        }
    } 

    $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;

    if ($sort) {
        
        $sortArray = explode(',', $sort);
        
        
        mysqli_query($link, 'START TRANSACTION');
        
        $sort_success = true;
        
        foreach($sortArray as $sort_order => $row_id) {
            $query_sort = "UPDATE especialidades SET ordem = $sort_order WHERE id = $row_id";
            if (!mysqli_query($link, $query_sort)) {
                $sort_success = false;
                break;
            }            
        }
        
        
        if ($sort_success) {
            mysqli_query($link, 'COMMIT');    
        } else {
            mysqli_query($link, 'ROLLBACK'); 
        }   
    }


?>

        <?php
    
    if ($del_id) {
        if ($affected_rows > 0) {
            echo '<div class="alert alert-success text-center" role="alert">Apagado com sucesso</div>';

        } 
        
    }
    

?>

            <div id="contentDiv" class="col-md-9" "col-sm-9" "col-xs-9">

                <br>

                <button id="btnSaveOrder" class="btn btn-default center-block" disabled>Guardar Nova Ordem</button>

                <br>

                <table id="tableGeral" class="table table-hover" class="align-center">
                    <thead>
                        <tr>
                            <th>Especialidades</th>
                            <td>
                                <h5>PREÇO</h5></td>
                        </tr>
                    </thead>
                    <?php
        
        $query_especialidades = 'SELECT * FROM especialidades ORDER by ordem ASC';
        $result_especialidades = mysqli_query($link, $query_especialidades);
        
                echo '<tbody>';  

                while ($row_especialidades = mysqli_fetch_array($result_especialidades)) {
             
                echo '<tr data-sort-element-id=' . $row_especialidades['id'] . '>';
            
                echo '<td>' . $row_especialidades['especialidades'] . '</td>';
                
                echo '<td>' . $row_especialidades['preco'] . '</td>';     
                    
                echo '<td class="text-right">';

                echo ' <a class="btn btn-info btnMove">Mover</a>  <button class="btn btn-danger button-del" type="button" data-del-id="'.$row_especialidades['id'].'">Apagar</button></td>';

                echo '</tr>';  
                    
        }
          
                echo '</tbody>';  
                 
        ?>
                </table>

                <a id="newEditBtn" class="btn btn-primary" href="especialidades_detail.php" role="button">Nova Especialidade</a>
                <br>
                <br>
            </div>
            <div style="clear: both;"></div>
            <?php include('footer.php') ?>